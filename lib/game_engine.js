const random_line = require("@scrum_project/random");

const line_draw = (i) => {
  let display = ""
  while (i > 0) {
    display += "_ ";
    i--;
  }
  return display;
}

//const newdraw = (word,guess) => {
//	for {word.length)
//		if (word.indexOf(i)){
//
//		}
//	}
//}

const startGame = () => {
  let word = random_line.getRandomWord();
  return {
    status: "RUNNING",
    word: word,
    lives: 5,
    display_word: line_draw(word.length),
    guesses: []
  };
};


const takeGuess = (game_state, guess) => {
  const guess_is_right = game_state.word.indexOf(guess) >= 0;
  if (!guess_is_right) {
    if (game_state.lives=1){
	console.log("Game Over");
	game_state.lives =0;
    }
    if (game_state.lives>1){
	game_state.lives--;
	game_state.guesses.push(guess);
    }
  }

  if (guess_is_right) {
        game_state.guesses.push(guess);
  }




  return game_state;
};


module.exports = {
  startGame,
  takeGuess
};
