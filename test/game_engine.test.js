const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
    let startGame = GameEngine.startGame()
    expect(startGame.word.length).toBeGreaterThan(3);
    expect(startGame.lives).toBe(5);
    expect(startGame.display_word).toContain("_");
    expect(startGame.display_word.length).toBe(2 * startGame.word.length);
    expect(startGame.guesses.length).toBe(0);
});
  test("update the guesses and lives when a wrong guess is given", ()=>{
    let game_state = GameEngine.startGame();
    expect(game_state.word.length).toBeGreaterThanOrEqual(3);
    let guess = GameEngine.takeGuess.guess;
    let new_game_state = GameEngine.takeGuess(game_state, guess);
    expect(new_game_state.lives).toBeLessThan(6);
    expect(new_game_state.guesses.length).toBeLessThan(5);
});

});
